from django.shortcuts import render
from django.conf import settings
from django.core.files.storage import FileSystemStorage

# Create your views here.
def index(request):
    context = {
        'Hello': 'hello',
    }
    return render(request, 'analysis/index.html', context)

def simple_upload(request):
    if request.method == 'POST' and request.FILES['myfile']:
        myfile = request.FILES['myfile']
        fs = FileSystemStorage()
        filename = fs.save(myfile.name, myfile)
        uploaded_file_url = fs.url(filename)
        return render(request, 'analysis/simple_upload.html', {
            'uploaded_file_url': uploaded_file_url
        })
    return render(request, 'analysis/simple_upload.html')